<?php

namespace Phalcon\OAuth2\Server\Http;

use InvalidArgumentException;
use Psr\Http\Message\StreamInterface;

abstract class HttpBase {
    /**
     * Protocol version
     *
     * @var string
     */
    protected $protocolVersion = '1.1';
    /**
     * A map of valid protocol versions
     *
     * @var array
     */
    protected static $validProtocolVersions = [
        '1.0' => true,
        '1.1' => true,
        '2.0' => true,
    ];

    /**
     * Body object
     *
     * @var \Psr\Http\Message\StreamInterface
     */
    protected $body;

    /*******************************************************************************
     * Protocol
     ******************************************************************************/

    /**
     * Retrieves the HTTP protocol version as a string.
     *
     * The string MUST contain only the HTTP version number (e.g., "1.1", "1.0").
     *
     * @return string HTTP protocol version.
     */
    public function getProtocolVersion(){
        return $this->protocolVersion;
    }

    /**
     * Return an instance with the specified HTTP protocol version.
     *
     * The version string MUST contain only the HTTP version number (e.g.,
     * "1.1", "1.0").
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new protocol version.
     *
     * @param string $version HTTP protocol version
     * @return static
     * @throws InvalidArgumentException if the http version is an invalid number
     */
    public function withProtocolVersion($version){
        if (!isset(self::$validProtocolVersions[$version])) {
            throw new InvalidArgumentException(
                'Invalid HTTP version. Must be one of: '
                . implode(', ', array_keys(self::$validProtocolVersions))
            );
        }
        $clone = clone $this;
        $clone->protocolVersion = $version;

        return $clone;
    }
    
    /**
     * Return an instance with the specified message body.
     *
     * The body MUST be a StreamInterface object.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return a new instance that has the
     * new body stream.
     *
     * @param StreamInterface $body Body.
     * @return static
     * @throws \InvalidArgumentException When the body is not valid.
     */
    public function withBody(StreamInterface $body){
        if($body instanceof StreamInterface ){
            $clone = clone $this;
            $clone->body = $body;
            return $clone;
        }else{
            throw new InvalidArgumentException("withBody requires instance of StreamInterface");
        }
    }

}