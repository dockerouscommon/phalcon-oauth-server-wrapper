<?php
namespace Phalcon\OAuth2\Server\Http;

use Phalcon\Http\Response as PhalconResponse;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Slim\Http\Stream;
use Slim\Http\Uri;
use Slim\Http\UploadedFile;
use Slim\Exception\InvalidMethodException;

class Response extends HttpBase implements ResponseInterface{
    
    protected $response;
    
    protected $headers;
    
    /**
     * Status code
     *
     * @var int
     */
    protected $status;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reasonPhrase = '';

    public function __construct(PhalconResponse $response){
        $this->response = $response;
        $this->headers = array();
        $this->withoutHeaders = array();
        $this->attributes = array();
    }
    

    /**
     * This method is applied to the cloned object
     * after PHP performs an initial shallow-copy. This
     * method completes a deep-copy by creating new objects
     * for the cloned object's internal reference pointers.
     */
    public function __clone(){
        if($this->body instanceof StreamInterface){
            $this->body = clone $this->body;
        }
    }

    /*******************************************************************************
     * Body
     ******************************************************************************/

    /**
     * Gets the body of the message.
     *
     * @return StreamInterface Returns the body as a stream.
     */
    public function getBody(){
        if($this->body instanceof StreamInterface ){
            return $this->body;
        }
        $stream = fopen('php://temp', 'w+');
        $this->body = new Stream($stream);
        $this->body->write($this->response->getContent());
        return $this->body;
    }

    /*******************************************************************************
     * Headers
     ******************************************************************************/

    /**
     * Retrieves all message header values.
     *
     * The keys represent the header name as it will be sent over the wire, and
     * each value is an array of strings associated with the header.
     *
     *     // Represent the headers as a string
     *     foreach ($message->getHeaders() as $name => $values) {
     *         echo $name . ": " . implode(", ", $values);
     *     }
     *
     *     // Emit headers iteratively:
     *     foreach ($message->getHeaders() as $name => $values) {
     *         foreach ($values as $value) {
     *             header(sprintf('%s: %s', $name, $value), false);
     *         }
     *     }
     *
     * While header names are not case-sensitive, getHeaders() will preserve the
     * exact case in which headers were originally specified.
     *
     * @return array Returns an associative array of the message's headers. Each
     *     key MUST be a header name, and each value MUST be an array of strings
     *     for that header.
     */
    public function getHeaders(){
        $headers = $this->response->getHeaders();
        return $headers->toArray();
    }

    /**
     * Checks if a header exists by the given case-insensitive name.
     *
     * @param string $name Case-insensitive header field name.
     * @return bool Returns true if any header names match the given header
     *     name using a case-insensitive string comparison. Returns false if
     *     no matching header name is found in the message.
     */
    public function hasHeader($name){
        $headers = $this->response->getHeaders();
        if($headers->get($name) === FALSE){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    /**
     * Retrieves a message header value by the given case-insensitive name.
     *
     * This method returns an array of all the header values of the given
     * case-insensitive header name.
     *
     * If the header does not appear in the message, this method MUST return an
     * empty array.
     *
     * @param string $name Case-insensitive header field name.
     * @return string[] An array of string values as provided for the given
     *    header. If the header does not appear in the message, this method MUST
     *    return an empty array.
     */
    public function getHeader($name){
        $headers = $this->response->getHeaders();
        $header = $headers->get($name);
        if($header === FALSE){
            return array();
        }
        return array($header);
    }

    /**
     * Retrieves a comma-separated string of the values for a single header.
     *
     * This method returns all of the header values of the given
     * case-insensitive header name as a string concatenated together using
     * a comma.
     *
     * NOTE: Not all header values may be appropriately represented using
     * comma concatenation. For such headers, use getHeader() instead
     * and supply your own delimiter when concatenating.
     *
     * If the header does not appear in the message, this method MUST return
     * an empty string.
     *
     * @param string $name Case-insensitive header field name.
     * @return string A string of values as provided for the given header
     *    concatenated together using a comma. If the header does not appear in
     *    the message, this method MUST return an empty string.
     */
    public function getHeaderLine($name){
        $headers = $this->response->getHeaders();
        $header = $headers->get($name);
        if($header === FALSE){
            return '';
        }
        return $header;
    }

    /**
     * Return an instance with the provided value replacing the specified header.
     *
     * While header names are case-insensitive, the casing of the header will
     * be preserved by this function, and returned from getHeaders().
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new and/or updated header and value.
     *
     * @param string $name Case-insensitive header field name.
     * @param string|string[] $value Header value(s).
     * @return static
     * @throws \InvalidArgumentException for invalid header names or values.
     */
    public function withHeader($name, $value){
        $clone = clone $this;
        $clone->response->setHeader($name,$value);
        return $clone;
    }

    /**
     * Return an instance with the specified header appended with the given value.
     *
     * Existing values for the specified header will be maintained. The new
     * value(s) will be appended to the existing list. If the header did not
     * exist previously, it will be added.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new header and/or value.
     *
     * @param string $name Case-insensitive header field name to add.
     * @param string|string[] $value Header value(s).
     * @return static
     * @throws \InvalidArgumentException for invalid header names or values.
     */
    public function withAddedHeader($name, $value){
        $clone = clone $this;
        $headers = $clone->response->getHeaders();
        $header = $headers->get($name);
        if($header === FALSE){
            $clone->response->setHeader($name,$value);
        }else{
            $clone->response->setHeader($name,$header.','.$value);
        }
        return $clone;
    }

    /**
     * Return an instance without the specified header.
     *
     * Header resolution MUST be done without case-sensitivity.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that removes
     * the named header.
     *
     * @param string $name Case-insensitive header field name to remove.
     * @return static
     */
    public function withoutHeader($name){
        $clone = clone $this;
        $headers = $clone->response->getHeaders();
        $headers->remove($name);
        $clone->response->setHeaders($headers);

        return $clone;
    }
    
    /*******************************************************************************
     * Status
     ******************************************************************************/

    
    /**
     * Gets the response status code.
     *
     * The status code is a 3-digit integer result code of the server's attempt
     * to understand and satisfy the request.
     *
     * @return int Status code.
     */
    public function getStatusCode(){
        if(empty($this->status)){
            $status = $this->response->getStatusCode();
            $this->status = $status[0];
        }
        return $this->status;
    }

    public function withStatus($code, $reasonPhrase = ''){
        $clone = clone $this;
        $clone->response->setStatusCode($code,$reasonPhrase);
        return $clone;
    }
    
    public function getReasonPhrase(){
        $status_array = explode(' ', $this->getStatusCode(), 2);
        if(isset($status_array[1])){
            return $status_array[1];
        } else {
            return '';
        }
    }
    
    public function getResponse(){
        return $this->response;
    }
}