<?php
namespace Phalcon\OAuth2\Server\Http;

use Phalcon\Http\Request as PhalconRequest;
use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;
use Slim\Http\Stream;
use Slim\Http\Uri;
use Slim\Http\UploadedFile;
use Slim\Exception\InvalidMethodException;

class Request extends HttpBase implements ServerRequestInterface{
    
    protected $request;
    
    protected $headers;
    
    protected $withoutHeaders;
    
    /**
     * The request method
     *
     * @var string
     */
    protected $method;

    /**
     * The request query string params
     *
     * @var array
     */
    protected $queryParams;

    /**
     * The request cookies
     *
     * @var array
     */
    protected $cookies;

    /**
     * The server environment variables at the time the request was created.
     *
     * @var array
     */
    protected $serverParams;

    /**
     * The request attributes (route segment names and values)
     *
     * @var array
     */
    protected $attributes;

    /**
     * The request URI target (path + query string)
     *
     * @var string
     */
    protected $requestTarget;

    /**
     * The request body parsed (if possible) into a PHP array or object
     *
     * @var null|array|object
     */
    protected $bodyParsed = false;

    /**
     * List of uploaded files
     *
     * @var UploadedFileInterface[]
     */
    protected $uploadedFiles;

    public function __construct(PhalconRequest $request){
        $this->request = $request;
        $this->headers = array();
        $this->withoutHeaders = array();
        $this->attributes = array();
    }
    

    /**
     * This method is applied to the cloned object
     * after PHP performs an initial shallow-copy. This
     * method completes a deep-copy by creating new objects
     * for the cloned object's internal reference pointers.
     */
    public function __clone(){
        if($this->body instanceof StreamInterface){
            $this->body = clone $this->body;
        }
    }

    /*******************************************************************************
     * Headers
     ******************************************************************************/
    
    public function hasHeader($name){
        if(key_exists($name, $this->headers)){
            return TRUE;
        }
        if(in_array($name, $this->withoutHeaders)){
            return FALSE;
        }
        $header = $this->request->getHeader($name);
        if(empty($header)){
            return FALSE;
        }
        return TRUE;
    }
    
    public function getHeader($name){
        if(!$this->hasHeader($name)){
            return array();
        }
        if(key_exists($name, $this->headers)){
            return $this->headers[$name];
        }
        $header = $this->request->getHeader($name);
        if(empty($header)){
            return array();
        }
        return array($header);
    }
    
    public function getHeaders(){
        $phalcon_headers = $this->request->getHeaders();
        $return_headers = array();
        foreach($phalcon_headers as $k => $v){
            $return_headers[$k] = array($v);
        }
        foreach($this->headers as $k => $v){
            $return_headers[$k] = array($v);
        }
        return $return_headers;
    }
    
    public function getHeaderLine($name){
        if(!$this->hasHeader($name)){
            return '';
        }
        $header = $this->getHeader($name);
        return implode(',', $header);
    }
    
    public function withHeader($name, $value){
        $clone = clone $this;
        if(($key = array_search($name, $clone->withoutHeaders)) !== false) {
            unset($clone->withoutHeaders[$key]);
        }
        if(is_array($value)){
            $clone->headers[$name] = $value;
        } else {
            $clone->headers[$name] = array($value);
        }
        return $clone;
    }
    
    public function withAddedHeader($name, $value){
        $clone = clone $this;
        if(($key = array_search($name, $clone->withoutHeaders)) !== false) {
            unset($clone->withoutHeaders[$key]);
        }
        if(!key_exists($name, $clone->headers)){
            $clone->headers[$name] = array();
        }
        if(is_array($value)){
            foreach ($value as $k => $v){
                $clone->headers[$name][] = $v;
            }
        } else {
            $clone->headers[$name][] = $value;
        }
        return $clone;
    }
    
    public function withoutHeader($name){
        $clone = clone $this;
        $clone->withoutHeaders[] = $name;
        if(key_exists($name, $clone->headers)){
            unset($clone->headers[$name]);
        }
        return $clone;
    }    
    
    /*******************************************************************************
     * Body
     ******************************************************************************/

    /**
     * Gets the body of the message.
     *
     * @return StreamInterface Returns the body as a stream.
     */
    public function getBody(){
        if($this->body instanceof StreamInterface ){
            return $this->body;
        }
        $stream = fopen('php://temp', 'w+');
        stream_copy_to_stream(fopen('php://input', 'r'), $stream);
        rewind($stream);
        $this->body = new Stream($stream);
        return $this->body;
    }

    public function getParsedBody(){
        if ($this->bodyParsed !== false) {
            return $this->bodyParsed;
        }
        if($this->request->isPost()){
            return $this->request->getPost();
        }
        $input = $this->request->getRawBody();
        $mediaType = $this->request->getMediaType();
        switch($mediaType){
            case 'application/json':
                $result = json_decode($input, true);
                if (!is_array($result)) {
                    return null;
                }
                break;
            case 'application/xml':
                $backup = libxml_disable_entity_loader(true);
                $backup_errors = libxml_use_internal_errors(true);
                $result = simplexml_load_string($input);
                libxml_disable_entity_loader($backup);
                libxml_clear_errors();
                libxml_use_internal_errors($backup_errors);
                if ($result === false) {
                    return null;
                }
                break;
            case 'text/xml':
                $backup = libxml_disable_entity_loader(true);
                $backup_errors = libxml_use_internal_errors(true);
                $result = simplexml_load_string($input);
                libxml_disable_entity_loader($backup);
                libxml_clear_errors();
                libxml_use_internal_errors($backup_errors);
                if ($result === false) {
                    return null;
                }
                break;
            default:
                return NULL;
        }
        $this->bodyParsed = $result;
        return $this->bodyParsed;
    }
    

    /**
     * Return an instance with the specified body parameters.
     *
     * These MAY be injected during instantiation.
     *
     * If the request Content-Type is either application/x-www-form-urlencoded
     * or multipart/form-data, and the request method is POST, use this method
     * ONLY to inject the contents of $_POST.
     *
     * The data IS NOT REQUIRED to come from $_POST, but MUST be the results of
     * deserializing the request body content. Deserialization/parsing returns
     * structured data, and, as such, this method ONLY accepts arrays or objects,
     * or a null value if nothing was available to parse.
     *
     * As an example, if content negotiation determines that the request data
     * is a JSON payload, this method could be used to create a request
     * instance with the deserialized parameters.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated body parameters.
     *
     * @param null|array|object $data The deserialized body data. This will
     *     typically be in an array or object.
     * @return static
     * @throws \InvalidArgumentException if an unsupported argument type is
     *     provided.
     */
    public function withParsedBody($data){
        if (!is_null($data) && !is_object($data) && !is_array($data)) {
            throw new InvalidArgumentException('Parsed body value must be an array, an object, or null');
        }

        $clone = clone $this;
        $clone->bodyParsed = $data;

        return $clone;
    }

    /**
     * Get request media type, if known.
     *
     * Note: This method is not part of the PSR-7 standard.
     *
     * @return string|null The request media type, minus content-type params
     */
    public function getMediaType()
    {
        $contentType = $this->request->getContentType();
        if ($contentType) {
            $contentTypeParts = preg_split('/\s*[;,]\s*/', $contentType);
            return strtolower($contentTypeParts[0]);
        }
        return null;
    }    

    /*******************************************************************************
     * Query Params
     ******************************************************************************/

    /**
     * Retrieve query string arguments.
     *
     * Retrieves the deserialized query string arguments, if any.
     *
     * Note: the query params might not be in sync with the URI or server
     * params. If you need to ensure you are only getting the original
     * values, you may need to parse the query string from `getUri()->getQuery()`
     * or from the `QUERY_STRING` server param.
     *
     * @return array
     */    
    
    public function getQueryParams(){
        if(!is_array($this->queryParams)){
            $this->queryParams = $this->request->getQuery();
        }
        return $this->queryParams;
    }
    

    /**
     * Return an instance with the specified query string arguments.
     *
     * These values SHOULD remain immutable over the course of the incoming
     * request. They MAY be injected during instantiation, such as from PHP's
     * $_GET superglobal, or MAY be derived from some other value such as the
     * URI. In cases where the arguments are parsed from the URI, the data
     * MUST be compatible with what PHP's parse_str() would return for
     * purposes of how duplicate query parameters are handled, and how nested
     * sets are handled.
     *
     * Setting query string arguments MUST NOT change the URI stored by the
     * request, nor the values in the server params.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated query string arguments.
     *
     * @param array $query Array of query string arguments, typically from
     *     $_GET.
     * @return static
     */
    public function withQueryParams(array $query){
        $clone = clone $this;
        $clone->queryParams = $query;

        return $clone;
    }

    /*******************************************************************************
     * Server Params
     ******************************************************************************/

    /**
     * Retrieve server parameters.
     *
     * Retrieves data related to the incoming request environment,
     * typically derived from PHP's $_SERVER superglobal. The data IS NOT
     * REQUIRED to originate from $_SERVER.
     *
     * @return array
     */
    public function getServerParams(){
        if(!is_array($this->serverParams)){
            $this->serverParams = $_SERVER;
        }
        return $this->serverParams;
    }
    
    /*******************************************************************************
     * Cookies
     ******************************************************************************/

    public function getCookieParams(){
        if(!is_array($this->cookies)){
            $this->cookies = $_COOKIE;
        }
        return $this->cookies;
    }
    
    /**
     * Return an instance with the specified cookies.
     *
     * The data IS NOT REQUIRED to come from the $_COOKIE superglobal, but MUST
     * be compatible with the structure of $_COOKIE. Typically, this data will
     * be injected at instantiation.
     *
     * This method MUST NOT update the related Cookie header of the request
     * instance, nor related values in the server params.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated cookie values.
     *
     * @param array $cookies Array of key/value pairs representing cookies.
     * @return static
     */
    public function withCookieParams(array $cookies){
        $clone = clone $this;
        $clone->cookies = $cookies;

        return $clone;
    }

    /*******************************************************************************
     * URI
     ******************************************************************************/

    /**
     * Retrieves the message's request target.
     *
     * Retrieves the message's request-target either as it will appear (for
     * clients), as it appeared at request (for servers), or as it was
     * specified for the instance (see withRequestTarget()).
     *
     * In most cases, this will be the origin-form of the composed URI,
     * unless a value was provided to the concrete implementation (see
     * withRequestTarget() below).
     *
     * If no URI is available, and no request-target has been specifically
     * provided, this method MUST return the string "/".
     *
     * @return string
     */
    public function getRequestTarget(){
        if ($this->requestTarget) {
            return $this->requestTarget;
        }
        
        $this->getUri();

        if ($this->uri === null) {
            return '/';
        }

        $basePath = $this->uri->getBasePath();
        $path = $this->uri->getPath();
        $path = $basePath . '/' . ltrim($path, '/');

        $query = $this->uri->getQuery();
        if ($query) {
            $path .= '?' . $query;
        }
        $this->requestTarget = $path;

        return $this->requestTarget;
    }


    /**
     * Return an instance with the specific request-target.
     *
     * If the request needs a non-origin-form request-target — e.g., for
     * specifying an absolute-form, authority-form, or asterisk-form —
     * this method may be used to create an instance with the specified
     * request-target, verbatim.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * changed request target.
     *
     * @link http://tools.ietf.org/html/rfc7230#section-2.7 (for the various
     *     request-target forms allowed in request messages)
     * @param mixed $requestTarget
     * @return static
     * @throws InvalidArgumentException if the request target is invalid
     */
    public function withRequestTarget($requestTarget){
        if (preg_match('#\s#', $requestTarget)) {
            throw new InvalidArgumentException(
                'Invalid request target provided; must be a string and cannot contain whitespace'
            );
        }
        $clone = clone $this;
        $clone->requestTarget = $requestTarget;

        return $clone;
    }

    /**
     * Retrieves the URI instance.
     *
     * This method MUST return a UriInterface instance.
     *
     * @link http://tools.ietf.org/html/rfc3986#section-4.3
     * @return UriInterface Returns a UriInterface instance
     *     representing the URI of the request.
     */
    public function getUri(){
        if($this->uri instanceof UriInterface ){
            return $this->uri;
        }
        $this->uri = Uri::createFromString($this->request->getURI());
        return $this->uri;
    }

    /**
     * Returns an instance with the provided URI.
     *
     * This method MUST update the Host header of the returned request by
     * default if the URI contains a host component. If the URI does not
     * contain a host component, any pre-existing Host header MUST be carried
     * over to the returned request.
     *
     * You can opt-in to preserving the original state of the Host header by
     * setting `$preserveHost` to `true`. When `$preserveHost` is set to
     * `true`, this method interacts with the Host header in the following ways:
     *
     * - If the the Host header is missing or empty, and the new URI contains
     *   a host component, this method MUST update the Host header in the returned
     *   request.
     * - If the Host header is missing or empty, and the new URI does not contain a
     *   host component, this method MUST NOT update the Host header in the returned
     *   request.
     * - If a Host header is present and non-empty, this method MUST NOT update
     *   the Host header in the returned request.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new UriInterface instance.
     *
     * @link http://tools.ietf.org/html/rfc3986#section-4.3
     * @param UriInterface $uri New request URI to use.
     * @param bool $preserveHost Preserve the original state of the Host header.
     * @return static
     */
    public function withUri(UriInterface $uri, $preserveHost = false){
        $clone = clone $this;
        $clone->uri = $uri;

        if (!$preserveHost) {
            if ($uri->getHost() !== '') {
                $clone->headers['Host'] = $uri->getHost();
            }
        } else {
            if ($uri->getHost() !== '' && (!$this->hasHeader('Host') || $this->getHeaderLine('Host') === '')) {
                $clone->headers['Host'] = $uri->getHost();
            }
        }

        return $clone;
    }
    /*******************************************************************************
     * Method
     ******************************************************************************/

    /**
     * Retrieves the HTTP method of the request.
     *
     * @return string Returns the request method.
     */
    public function getMethod(){
        if ($this->method === null) {
            $this->method = $this->request->getMethod();
        }
        return $this->method;
    }

    /**
     * Return an instance with the provided HTTP method.
     *
     * While HTTP method names are typically all uppercase characters, HTTP
     * method names are case-sensitive and thus implementations SHOULD NOT
     * modify the given string.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * changed request method.
     *
     * @param string $method Case-sensitive method.
     * @return static
     * @throws \InvalidArgumentException for invalid HTTP methods.
     */
    public function withMethod($method){
        if($this->request->isValidHttpMethod($method)){
            $clone = clone $this;
            $clone->method = $method;
        }else{
            throw new InvalidMethodException($this, $method);
        }

        return $clone;
    }

    /*******************************************************************************
     * File Params
     ******************************************************************************/

    /**
     * Retrieve normalized file upload data.
     *
     * This method returns upload metadata in a normalized tree, with each leaf
     * an instance of Psr\Http\Message\UploadedFileInterface.
     *
     * These values MAY be prepared from $_FILES or the message body during
     * instantiation, or MAY be injected via withUploadedFiles().
     *
     * @return array An array tree of UploadedFileInterface instances; an empty
     *     array MUST be returned if no data is present.
     */
    public function getUploadedFiles(){
        if(is_array($this->uploadedFiles)){
            return $this->uploadedFiles;
        }
        $phalconFiles = $this->request->getUploadedFiles();
        $this->uploadedFiles = array();
        foreach ($phalconFiles as $k => $v){
            $this->uploadedFiles[] = new UploadedFile(
                    $v->getTempName(),
                    $v->getName(),
                    $v->getRealType(),
                    $v->getSize(),
                    $v->getError()
                    );
        }
        return $this->uploadedFiles;
    }

    /**
     * Create a new instance with the specified uploaded files.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated body parameters.
     *
     * @param array $uploadedFiles An array tree of UploadedFileInterface instances.
     * @return static
     * @throws \InvalidArgumentException if an invalid structure is provided.
     */
    public function withUploadedFiles(array $uploadedFiles){
        $clone = clone $this;
        $clone->uploadedFiles = $uploadedFiles;

        return $clone;
    }

    /*******************************************************************************
     * Attributes
     ******************************************************************************/

    /**
     * Retrieve attributes derived from the request.
     *
     * The request "attributes" may be used to allow injection of any
     * parameters derived from the request: e.g., the results of path
     * match operations; the results of decrypting cookies; the results of
     * deserializing non-form-encoded message bodies; etc. Attributes
     * will be application and request specific, and CAN be mutable.
     *
     * @return array Attributes derived from the request.
     */
    public function getAttributes()
    {
        return $this->attributes->all();
    }

    /**
     * Retrieve a single derived request attribute.
     *
     * Retrieves a single derived request attribute as described in
     * getAttributes(). If the attribute has not been previously set, returns
     * the default value as provided.
     *
     * This method obviates the need for a hasAttribute() method, as it allows
     * specifying a default value to return if the attribute is not found.
     *
     * @see getAttributes()
     * @param string $name The attribute name.
     * @param mixed $default Default value to return if the attribute does not exist.
     * @return mixed
     */
    public function getAttribute($name, $default = null){
        if(key_exists($name, $this->attributes)){
            return $this->attributes[$name];
        }
        return $default;
    }

    /**
     * Return an instance with the specified derived request attribute.
     *
     * This method allows setting a single derived request attribute as
     * described in getAttributes().
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated attribute.
     *
     * @see getAttributes()
     * @param string $name The attribute name.
     * @param mixed $value The value of the attribute.
     * @return static
     */
    public function withAttribute($name, $value){
        $clone = clone $this;
        $clone->attributes[$name] = $value;

        return $clone;
    }

    /**
     * Create a new instance with the specified derived request attributes.
     *
     * Note: This method is not part of the PSR-7 standard.
     *
     * This method allows setting all new derived request attributes as
     * described in getAttributes().
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return a new instance that has the
     * updated attributes.
     *
     * @param  array $attributes New attributes
     * @return static
     */
    public function withAttributes(array $attributes)
    {
        $clone = clone $this;
        $clone->attributes = $attributes;

        return $clone;
    }

    /**
     * Return an instance that removes the specified derived request attribute.
     *
     * This method allows removing a single derived request attribute as
     * described in getAttributes().
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that removes
     * the attribute.
     *
     * @see getAttributes()
     * @param string $name The attribute name.
     * @return static
     */
    public function withoutAttribute($name){
        $clone = clone $this;
        if(key_exists($name, $clone->attributes)){
            unset($clone->attributes[$name]);
        }

        return $clone;
    }
}