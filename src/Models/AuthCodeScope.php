<?php

namespace Phalcon\OAuth2\Server\Models;

/**
 * Class AuthCodeScope
 * @package Phalcon\OAuth2\Server\Models
 */

/*

CREATE TABLE oauth_auth_code_scopes (
    identifier INT NOT NULL AUTO_INCREMENT,
    authCode VARCHAR(64) NOT NULL,
    scope VARCHAR(64) NOT NULL,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    PRIMARY KEY ( identifier )
    );
INSERT INTO oauth_auth_code_scopes (authCode, scope, created_at, updated_at)
VALUES ('changethisauthcode', 'testscope', UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()));

 */

class AuthCodeScope extends OAuth
{

    /**
     *
     * @var integer
     */
    public $identifier;

    /**
     *
     * @var string
     */
    public $authCode;

    /**
     *
     * @var string
     */
    public $scope;

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AuthCodeScope[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AuthCodeScope
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('authCode', AuthCode::class, 'identifier');
        $this->belongsTo('scope', Scope::class, 'identifier');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oauth_auth_code_scopes';
    }

}
