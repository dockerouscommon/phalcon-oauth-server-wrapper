<?php

namespace Phalcon\OAuth2\Server\Models;

/**
 * Class AccessToken
 * @package Phalcon\OAuth2\Server\Models
 * 
 * @source https://github.com/nueko/phalcon-oauth2-server
 */

/*

CREATE TABLE oauth_access_tokens (
    identifier VARCHAR(64) NOT NULL,
    oauthclient INT NOT NULL,
    oauthuser INT,
    expiryDateTime DATETIME NOT NULL,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    PRIMARY KEY ( identifier )
    );

 */

use Phalcon\OAuth2\Server\Models\AccessTokenScope;
use Phalcon\OAuth2\Server\Models\RefreshToken;
use Phalcon\OAuth2\Server\Models\OAuthUser;
use Phalcon\OAuth2\Server\Models\OAuthClient;

class AccessToken extends OAuth
{

    /**
     *
     * @var string
     */
    public $identifier;
    
    /**
     *
     * @var string
     */
    public $expiryDateTime;

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AccessToken[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AccessToken
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        //$this->hasMany('id', AccessTokenScope::class, 'access_token_id');
        //$this->belongsTo('session_id', Session::class, 'id');
        $this->hasMany('identifier', RefreshToken::class, 'accessToken');
        $this->hasManytoMany('identifier', AccessTokenScope::class, 'accessToken', 'scope', Scope::class, 'identifier');
        $this->belongsTo('oauthclient', OAuthClient::class, 'identifier');
        $this->belongsTo('oauthuser', OAuthUser::class, 'identifier');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oauth_access_tokens';
    }
    
    public function addScopes($scopes = []){
        if(!is_array($scopes)){
            return false;
        }
        $add_scopes = [];
        foreach($scopes as $scope){
            $scope_model = Scope::findFirst([
                "identifier = :identifier: ",
                'bind' =>[
                    "identifier" => $scope,
                ]
            ]);
            if($scope_model !== FALSE){
                $add_scopes[] = $scope;
            }
        }
        $accessTokenScopes = AccessTokenScope::find([
            "accessToken = :accessToken:",
            'bind' =>[
                "accessToken" => $this->identifier
            ]
        ]);
        $existing_scopes = array();
        foreach ($accessTokenScopes as $accessTokenScope){
            if(!in_array($accessTokenScope->scope, $add_scopes)){
                $accessTokenScope->delete();
            }else{
                $existing_scopes[] = $accessTokenScope->scope;
            }
        }
        foreach($add_scopes as $add_scope){
            if(!in_array($add_scope, $existing_scopes)){
                $accessTokenScope = new AccessTokenScope();
                $accessTokenScope->scope = $add_scope;
                $accessTokenScope->accessToken = $this->identifier;
                $accessTokenScope->save();
            }
        }
        
    }
    
    public function deleteScopes(){
        $accessTokenScopes = AccessTokenScope::find([
            "accessToken = :accessToken:",
            'bind' =>[
                "accessToken" => $this->identifier
            ]
        ]);
        foreach ($accessTokenScopes as $accessTokenScope){
            $accessTokenScope->delete();
        }
    }

    public function deleteRefreshTokens(){
        $refreshTokens = RefreshToken::find([
            "accessToken = :accessToken:",
            'bind' =>[
                "accessToken" => $this->identifier
            ]
        ]);
        foreach ($refreshTokens as $refreshToken){
            $refreshToken->delete();
        }
    }

}
