<?php

namespace Phalcon\OAuth2\Server\Models;

/**
 * Class RefreshToken
 * @package Phalcon\OAuth2\Server\Models
 * 
 * @source https://github.com/nueko/phalcon-oauth2-server
 */

/*

CREATE TABLE oauth_refresh_tokens (
    identifier VARCHAR(64) NOT NULL,
    accessToken VARCHAR(64) NOT NULL,
    expiryDateTime DATETIME NOT NULL,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    PRIMARY KEY ( identifier )
    );

 */

class RefreshToken extends OAuth
{

    /**
     *
     * @var string
     */
    public $identifier;

    /**
     *
     * @var string
     */
    public $accessToken;

    /**
     *
     * @var string
     */
    public $expiryDateTime;

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefreshToken[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefreshToken
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('accessToken', AccessToken::class, 'identifier');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oauth_refresh_tokens';
    }

}
