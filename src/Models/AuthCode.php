<?php

namespace Phalcon\OAuth2\Server\Models;

/**
 * Class AuthCode
 * @package Phalcon\OAuth2\Server\Models
 * 
 * @source https://github.com/nueko/phalcon-oauth2-server
 */

/*

CREATE TABLE oauth_auth_codes (
    identifier VARCHAR(64) NOT NULL,
    oauthclient INT NOT NULL,
    oauthuser INT NOT NULL,
    expiryDateTime DATETIME NOT NULL,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    PRIMARY KEY ( identifier )
    );
INSERT INTO oauth_auth_codes (identifier, oauthclient, oauthuser, expiryDateTime, created_at, updated_at)
VALUES ('changethisauthcode', 1, 1, NOW() + INTERVAL 1 DAY, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()));

 */

use Phalcon\OAuth2\Server\Models\OAuthUser;
use Phalcon\OAuth2\Server\Models\OAuthClient;
use Phalcon\OAuth2\Server\Models\AuthCodeScope;

class AuthCode extends OAuth{

    /**
     *
     * @var string
     */
    public $identifier;

    /**
     *
     * @var string
     */
    public $redirectUri;

    /**
     *
     * @var string
     */
    public $expiryDateTime;

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AuthCode[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AuthCode
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasManytoMany('identifier', AuthCodeScope::class, 'authCode', 'scope', Scope::class, 'identifier');
        $this->belongsTo('oauthclient', OAuthClient::class, 'identifier');
        $this->belongsTo('oauthuser', OAuthUser::class, 'identifier');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oauth_auth_codes';
    }

    public function addScopes($scopes = []){
        if(!is_array($scopes)){
            return false;
        }
        $add_scopes = [];
        foreach($scopes as $scope){
            $scope_model = Scope::findFirst([
                "identifier = :identifier: ",
                'bind' =>[
                    "identifier" => $scope,
                ]
            ]);
            if($scope_model !== FALSE){
                $add_scopes[] = $scope;
            }
        }
        $authCodeScopes = AuthCodeScope::find([
            "authCode = :authCode:",
            'bind' =>[
                "authCode" => $this->identifier
            ]
        ]);
        $existing_scopes = array();
        foreach ($authCodeScopes as $authCodeScope){
            if(!in_array($authCodeScope->scope, $add_scopes)){
                $authCodeScope->delete();
            }else{
                $existing_scopes[] = $authCodeScope->scope;
            }
        }
        foreach($add_scopes as $add_scope){
            if(!in_array($add_scope, $existing_scopes)){
                $authCodeScope = new AccessTokenScope();
                $authCodeScope->scope = $add_scope;
                $authCodeScope->authCode = $this->identifier;
                $authCodeScope->save();
            }
        }
        
    }
    
    public function deleteScopes(){
        $authCodeScopes = AuthCodeScope::find([
            "authCode = :authCode:",
            'bind' =>[
                "authCode" => $this->identifier
            ]
        ]);
        foreach ($authCodeScopes as $authCodeScope){
            $authCodeScope->delete();
        }
    }

}
