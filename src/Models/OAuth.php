<?php

namespace Phalcon\OAuth2\Server\Models;

use Phalcon\Mvc\Model;

/**
 * Class OAuth
 * @package Phalcon\OAuth2\Server\Models
 * 
 * @source https://github.com/nueko/phalcon-oauth2-server
 */
abstract class OAuth extends Model
{

    /**
     *
     * @var integer
     */
    public $created_at;

    /**
     *
     * @var integer
     */
    public $updated_at;

    /**
     * Insert value for created and updated at column
     */
    public function beforeValidationOnCreate(){
        $this->created_at = time();
        $this->updated_at = time();
    }

    /**
     * Update value for updated at column
     */
    public function beforeValidationOnUpdate()
    {
        $this->updated_at = time();
    }
    
    public function initialize(){
        $this->setSource($this->getSource());
    }

}