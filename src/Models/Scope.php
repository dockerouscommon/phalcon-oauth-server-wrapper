<?php

namespace Phalcon\OAuth2\Server\Models;

/**
 * Class Scope
 * @package Phalcon\OAuth2\Server\Models
 * 
 * @source https://github.com/nueko/phalcon-oauth2-server
 */

/*

CREATE TABLE oauth_scopes (
    identifier VARCHAR(64) NOT NULL,
    description VARCHAR(256),
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    PRIMARY KEY ( identifier )
    );
INSERT INTO oauth_scopes (identifier, description, created_at, updated_at)
VALUES ('testscope', 'This scope is for testing purposes only', UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()));

 */

class Scope extends OAuth
{

    /**
     *
     * @var string
     */
    public $identifier;

    /**
     *
     * @var string
     */
    public $description;

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Scope[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Scope
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasManytoMany('identifier', AccessTokenScope::class, 'scope', 'accessToken', AccessToken::class, 'identifier');
        $this->hasManytoMany('identifier', AuthCodeScope::class, 'scope', 'authCode', AuthCode::class, 'identifier');
        //$this->hasMany('id', oauthClientScope::class, 'scope_id');
        //$this->hasMany('id', GrantScope::class, 'scope_id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oauth_scopes';
    }

}
