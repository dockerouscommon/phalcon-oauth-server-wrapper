<?php

namespace Phalcon\OAuth2\Server\Models;

/**
 * Class oauthClient
 * @package Phalcon\OAuth2\Server\Models
 * 
 * @source https://github.com/nueko/phalcon-oauth2-server
 */

/*

CREATE TABLE oauth_clients (
    identifier INT NOT NULL AUTO_INCREMENT,
    secret VARCHAR(64) NOT NULL,
    name VARCHAR(128) NOT NULL UNIQUE,
    redirectUri VARCHAR(256) NOT NULL,
    isConfidential TINYINT DEFAULT 1,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    PRIMARY KEY ( identifier )
    );
ALTER TABLE oauth_clients AUTO_INCREMENT = 260000;
INSERT INTO oauth_clients (secret, name, redirectUri, isConfidential, created_at, updated_at)
VALUES ('$2y$10$jxr.iiGn.jz0KJMsOWJg1uS3AfcQaMZz.2pMPI6.I1lTIPsSOR1Me', 'Test Client', 'http://www.example.com/return.php', 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()));

secret is changethissecret
 */

use Phalcon\OAuth2\Server\Models\AccessToken;
use Phalcon\OAuth2\Server\Models\AuthCode;

class OAuthClient extends OAuth{

    /**
     *
     * @var integer
     */
    public $identifier;

    /**
     *
     * @var string
     */
    public $secret;

    /**
     *
     * @var string
     */
    public $name;

    /**
     * @var string|string[]
     */
    public $redirectUri;

    /**
     * @var integer
     */
    protected $isConfidential;

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return OAuthClient[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return OAuthClient
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasMany('identifier', AccessToken::class, 'oauthclient');
        $this->hasMany('identifier', AuthCode::class, 'oauthclient');
//        $this->hasMany('id', ClientEndpoint::class, 'client_id');
//        $this->hasMany('id', ClientGrant::class, 'client_id');
//        $this->hasMany('id', ClientScope::class, 'client_id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oauth_clients';
    }

    public function setIsConfidential($confidential = TRUE){
        if($confidential === FALSE || $confidential === 0){
            $this->isConfidential = 0;
        } else {
            $this->isConfidential = 1;
        }
    }
    
    public function getIsConfidential(){
        if($this->isConfidential === 0){
            return FALSE;
        }
        return TRUE;
    }
    
    public function columnMap(){
        return [
            "identifier"=>"identifier",
            "secret"=>"secret",
            "name"=>"name",
            "redirectUri"=>"redirectUri",
            "isConfidential"=>"isConfidential",
            "created_at"=>"updated_at",
            "updated_at"=>"created_at",
        ];
    }
}
