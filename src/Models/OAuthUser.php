<?php

namespace Phalcon\OAuth2\Server\Models;

/**
 * Class AccessToken
 * @package Phalcon\OAuth2\Server\Models
 * 
 * @source https://github.com/nueko/phalcon-oauth2-server
 */

/*

CREATE TABLE oauth_users (
    identifier INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(64) NOT NULL UNIQUE,
    password VARCHAR(256) NOT NULL,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    PRIMARY KEY ( identifier )
    );
ALTER TABLE oauth_users AUTO_INCREMENT = 260000;
INSERT INTO oauth_users (username, password, created_at, updated_at)
VALUES ('testuser', 'changethispassword', UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()));

 */

use Phalcon\OAuth2\Server\Models\AccessToken;
use Phalcon\OAuth2\Server\Models\AuthCode;

class OAuthUser extends OAuth{

    /**
     *
     * @var integer
     */
    public $identifier;

    /**
     *
     * @var string
     */
    public $username;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AccessToken[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AccessToken
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasMany('identifier', AccessToken::class, 'oauthuser');
        $this->hasMany('identifier', AuthCode::class, 'oauthuser');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oauth_users';
    }
    
    public function setPassword($password, $no_hash = FALSE){
        if(empty($password) || !is_string($password)){
            // Throw bad password exception
        }
        if($no_hash === TRUE){
            $this->password = $password;
        }else{
            $this->password = password_hash($password, PASSWORD_BCRYPT);
        }
        return $this->password;
    }
    
    public function getPassword(){
        return $this->password;
    }

}
