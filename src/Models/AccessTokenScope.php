<?php

namespace Phalcon\OAuth2\Server\Models;

/**
 * Class AccessTokenScope
 * @package Phalcon\OAuth2\Server\Models
 */

/*

CREATE TABLE oauth_access_token_scopes (
    identifier INT NOT NULL AUTO_INCREMENT,
    accessToken VARCHAR(64) NOT NULL,
    scope VARCHAR(64) NOT NULL,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    PRIMARY KEY ( identifier )
    );

 */

class AccessTokenScope extends OAuth
{

    /**
     *
     * @var integer
     */
    public $identifier;

    /**
     *
     * @var string
     */
    public $accessToken;

    /**
     *
     * @var string
     */
    public $scope;

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AccessTokenScope[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AccessTokenScope
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('accessToken', AccessToken::class, 'identifier');
        $this->belongsTo('scope', Scope::class, 'identifier');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oauth_access_token_scopes';
    }

}
