<?php


namespace Phalcon\OAuth2\Server;

use Phalcon\Mvc\User\Component;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\ResourceServer;
use League\OAuth2\Server\Grant\AuthCodeGrant;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use League\OAuth2\Server\Grant\ImplicitGrant;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\Exception\OAuthServerException;
use Phalcon\OAuth2\Server\Repositories\AccessTokenRepository;
use Phalcon\OAuth2\Server\Repositories\AuthCodeRepository;
use Phalcon\OAuth2\Server\Repositories\OAuthClientRepository;
use Phalcon\OAuth2\Server\Repositories\RefreshTokenRepository;
use Phalcon\OAuth2\Server\Repositories\ScopeRepository;
use Phalcon\OAuth2\Server\Repositories\OAuthUserRepository;
use Phalcon\OAuth2\Server\Http\Request;
use Phalcon\OAuth2\Server\Http\Response;

class OAuthManager extends Component{
    
    /**
     * The authorization server
     *
     * @var AuthorizationServer
     */
    protected $authorization;

    /**
     * The resource server.
     *
     * @var ResourceServer
     */
    protected $resource;
    
    /**
     * The resource server.
     *
     * @var ResourceServer
     */
    protected $request;
    
    /**
     * The resource server.
     *
     * @var ResourceServer
     */
    protected $response;
    
    protected $accessToken;
    protected $authCode;
    protected $oauthClient;
    protected $refreshToken;
    protected $scope;
    protected $oauthuser;
    protected $privateKey;
    protected $publicKey;
    
    protected $oauth_access_token_id;
    protected $oauth_client_id;
    protected $oauth_user_id;
    protected $oauth_scopes;

    public function __construct($privateKey, $publicKey){
        $this->accessToken = new AccessTokenRepository();
        $this->authCode = new AuthCodeRepository();
        $this->oauthClient = new OAuthClientRepository();
        $this->refreshToken = new RefreshTokenRepository();
        $this->scope = new ScopeRepository();
        $this->oauthuser = new OAuthUserRepository();
        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;
        $this->authorization = new AuthorizationServer(
                $this->oauthClient,
                $this->accessToken,
                $this->scope,
                $this->privateKey,
                $this->publicKey
                );
        $this->resource = new ResourceServer($this->accessToken, $this->publicKey);
        $this->request = new Request($this->getDi()->get('request'));
        $this->response = new Response($this->getDi()->get('response'));
    }
     
    /**
     * @param null $params
     *
     * @return \Phalcon\Mvc\Model\Query\BuilderInterface
     */
    public function getBuilder($params = null){
        return $this->modelsManager->createBuilder($params);
    }
    
    public function enableAuthCodeGrant(\DateInterval $accessTokenTTL = null, \DateInterval $authCodeTTL = null){
        if ($authCodeTTL instanceof \DateInterval === false) {
            $authCodeTTL = new \DateInterval('PT1H');
        }
        $this->authorization->enableGrantType(
            new AuthCodeGrant(
                $this->authCode,
                $this->refreshToken,
                $authCodeTTL
            ),
            $accessTokenTTL
        );
    }
    
    public function enableOAuthClientGrant(\DateInterval $accessTokenTTL = null){
        $this->authorization->enableGrantType(
            new ClientCredentialsGrant(),
            $accessTokenTTL
        );
    }
    
    public function enableImplicitGrant(\DateInterval $accessTokenTTL = null){
        if ($accessTokenTTL instanceof \DateInterval === false) {
            $accessTokenTTL = new \DateInterval('PT1H');
        }
        $this->authorization->enableGrantType(
            new ImplicitGrant($accessTokenTTL),
            $accessTokenTTL
        );
    }
    
    public function enablePasswordGrant(\DateInterval $accessTokenTTL = null, \DateInterval $refreshTokenTTL = null){
        if ($refreshTokenTTL instanceof \DateInterval === false) {
            $refreshTokenTTL = new \DateInterval('P1M');
        }
        $grant = new PasswordGrant(
            $this->oauthuser,           // instance of UserRepositoryInterface
            $this->refreshToken    // instance of RefreshTokenRepositoryInterface
        );
        $grant->setRefreshTokenTTL($refreshTokenTTL); // refresh tokens will expire after 1 month

        // Enable the password grant on the server with a token TTL of 1 hour
        $this->authorization->enableGrantType(
            $grant,
            $accessTokenTTL // access tokens will expire after 1 hour
        );
    }
    
    public function enableRefreshTokenGrant(\DateInterval $accessTokenTTL = null, \DateInterval $refreshTokenTTL = null){
        if ($refreshTokenTTL instanceof \DateInterval === false) {
            $refreshTokenTTL = new \DateInterval('P1M');
        }
        $grant = new RefreshTokenGrant(
            $this->refreshToken    // instance of RefreshTokenRepositoryInterface
        );
        $grant->setRefreshTokenTTL($refreshTokenTTL); // refresh tokens will expire after 1 month

        // Enable the password grant on the server with a token TTL of 1 hour
        $this->authorization->enableGrantType(
            $grant,
            $accessTokenTTL // access tokens will expire after 1 hour
        );
    }
    
//    public function getAuthorization() {
//        return $this->authorization;
//    }
//    
//    public function getResource(){
//        return $this->resource;
//    }
    public function validateAuthorizationRequest(){
        return $this->authorization->validateAuthorizationRequest($this->request);
    }
    
    public function completeAuthorizationRequest(AuthorizationRequest $authRequest){
        $authorizationResponse = $this->authorization->completeAuthorizationRequest($authRequest, $this->response);
        $phalconResponse = $authorizationResponse->getResponse();
        $phalconResponse->setContent((string)$authorizationResponse->getBody());
        return $phalconResponse;
    }
    
    public function respondToAccessTokenRequest(){
        $accessTokenResponse = $this->authorization->respondToAccessTokenRequest($this->request, $this->response);
        $phalconResponse = $accessTokenResponse->getResponse();
        $phalconResponse->setContent((string)$accessTokenResponse->getBody());
        return $phalconResponse;
    }
    
    public function validateAuthenticatedRequest(){
        $authenticatedRequest = $this->resource->validateAuthenticatedRequest($this->request);
        $this->oauth_access_token_id = $authenticatedRequest->getAttribute('oauth_access_token_id');
        $this->oauth_client_id = $authenticatedRequest->getAttribute('oauth_client_id');
        $this->oauth_user_id = $authenticatedRequest->getAttribute('oauth_user_id');
        $this->oauth_scopes = $authenticatedRequest->getAttribute('oauth_scopes');
    }
    
    public function getExceptionResponse($exception){
        if($exception instanceof OAuthServerException){
            $exceptionResponse = $exception->generateHttpResponse($this->response);
            $phalconResponse = $exceptionResponse->getResponse();
            $phalconResponse->setContent((string)$exceptionResponse->getBody());
            return $phalconResponse;
        }
    }
}