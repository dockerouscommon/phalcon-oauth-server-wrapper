<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Phalcon\OAuth2\Server\Repositories;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Phalcon\OAuth2\Server\Entities\ScopeEntity;
use Phalcon\OAuth2\Server\Models\Scope;

class ScopeRepository implements ScopeRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getScopeEntityByIdentifier($scopeIdentifier)
    {
        $scope = Scope::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $scopeIdentifier
                    ]
                ]);
        if($scope === FALSE){
            return;
        }

        $scopeEntity = new ScopeEntity();
        $scopeEntity->setIdentifier($scope->identifier);
        $scopeEntity->setDescription($scope->description);

        return $scopeEntity;
    }

    /**
     * {@inheritdoc}
     */
    public function finalizeScopes(
        array $scopes,
        $grantType,
        ClientEntityInterface $oauthClientEntity,
        $oauthUserIdentifier = null
    ) {
        // Example of programatically modifying the final scope of the access token
//        if ((int) $oauthUserIdentifier === 1) {
//            $scope = new ScopeEntity();
//            $scope->setIdentifier('email');
//            $scopes[] = $scope;
//        }

        return $scopes;
    }
}
