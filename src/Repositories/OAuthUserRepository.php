<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Phalcon\OAuth2\Server\Repositories;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Phalcon\OAuth2\Server\Entities\OAuthUserEntity;
use Phalcon\OAuth2\Server\Models\OAuthUser;

class OAuthUserRepository implements UserRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getUserEntityByUserCredentials(
        $username,
        $password,
        $grantType,
        ClientEntityInterface $oauthClientEntity
    ) {
        $oauthUser = OAuthUser::findFirst([
                    "username = :username:",
                    'bind' =>[
                        "username" => $username
                    ]
                ]);
        if($oauthUser === FALSE){
            return;
        }
        if (!password_verify($password, $oauthUser->getPassword())) {
            return;
        }
        // Todo: Verify OauthUser can use grant type
        // Todo: Verify OauthUser can use oauthClient
        $oauthUserEntity = new OAuthUserEntity();
        $oauthUserEntity->setPassword($oauthUser->getPassword(), TRUE);
        $oauthUserEntity->setIdentifier($oauthUser->identifier);
        $oauthUserEntity->setUsername($oauthUser->oauthusername);
        return $oauthUserEntity;
    }
}
