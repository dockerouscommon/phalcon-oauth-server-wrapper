<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Phalcon\OAuth2\Server\Repositories;

use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use Phalcon\OAuth2\Server\Entities\AuthCodeEntity;
use Phalcon\OAuth2\Server\Models\OAuthClient;
use Phalcon\OAuth2\Server\Models\OAuthUser;

class AuthCodeRepository implements AuthCodeRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity)
    {
        // Some logic to persist the auth code to a database
        $authCode = AuthCode::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $authCodeEntity->getIdentifier()
                    ]
                ]);
        if($authCode === FALSE){
            $authCode = new AuthCode();
            $authCode->identifier = $authCodeEntity->getIdentifier();
        }
        $expiryDateTime = $authCodeEntity->getExpiryDateTime();
        if($expiryDateTime instanceof \DateTime){
            $authCode->expiryDateTime = $expiryDateTime->format('Y-m-d H:i:s');
        }else{
            //Throw invalid expiry error
        }
        $authCode->redirectUri = $authCodeEntity->getRedirectUri();
        
        $oauthUserIdentifier = $authCodeEntity->getUserIdentifier();
        $oauthUser = OAuthUser::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $oauthUserIdentifier
                    ]
                ]);
        if($oauthUser ===FALSE){
            // Throw Error No OauthUser Found
        }else{
            $authCode->oauthuser = $oauthUser->identifier;
        }

        $oauthClientEntity = $authCodeEntity->getClient();
        $oauthClient = OAuthClient::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $oauthClientEntity->getIdentifier()
                    ]
                ]);
        if($oauthClient ===FALSE){
            // Throw Error No oauthClient Found
        }else{
            $authCode->oauthclient = $oauthClient->identifier;
        }
        
        $authCode->save();
        
        $scopeEntities = $authCodeEntity->getScopes();
        $scopes = array();
        foreach ($scopeEntities as $scopeEntity){
            $scopes[] = $scopeEntity->getIdentifier();
        }
        $authCode->addScopes($scopes);
    }

    /**
     * {@inheritdoc}
     */
    public function revokeAuthCode($codeId)
    {
        // Some logic to revoke the auth code in a database
        $authCode = AuthCode::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $codeId
                    ]
                ]);
        if($authCode === FALSE){
            return TRUE;
        }
        $authCode->deleteScopes();
        $authCode->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function isAuthCodeRevoked($codeId)
    {
        $authCode = AuthCode::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $codeId
                    ]
                ]);
        if($authCode === FALSE){
            return TRUE;
        }
        return false; // The auth code has not been revoked
    }

    /**
     * {@inheritdoc}
     */
    public function getNewAuthCode()
    {
        return new AuthCodeEntity();
    }
}
