<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Phalcon\OAuth2\Server\Repositories;

use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Phalcon\OAuth2\Server\Entities\AccessTokenEntity;
use Phalcon\OAuth2\Server\Models\AccessToken;
use Phalcon\OAuth2\Server\Models\OAuthClient;
use Phalcon\OAuth2\Server\Models\OAuthUser;

class AccessTokenRepository implements AccessTokenRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity){
        $accessToken = new AccessToken();
        $accessToken->identifier = $accessTokenEntity->getIdentifier();
        $expiryDateTime = $accessTokenEntity->getExpiryDateTime();
        if($expiryDateTime instanceof \DateTime){
            $accessToken->expiryDateTime = $expiryDateTime->format('Y-m-d H:i:s');
        }else{
            //Throw invalid expiry error
        }
        // Some logic here to save the access token to a database
        $oauthUserIdentifier = $accessTokenEntity->getUserIdentifier();
        if($oauthUserIdentifier !== NULL){
            $oauthUser = OAuthUser::findFirst([
                        "identifier = :identifier:",
                        'bind' =>[
                            "identifier" => $oauthUserIdentifier
                        ]
                    ]);
            if($oauthUser instanceof OAuthUser){
                $accessToken->oauthuser = $oauthUser->identifier;
    //            $accessTokens = AccessToken::find([
    //                        "oauthuser = :oauthuser:",
    //                        'bind' =>[
    //                            "oauthuser" => $oauthUser->getIdentifier(),
    //                        ]
    //                    ]);
            }
        }
        $oauthClientEntity = $accessTokenEntity->getClient();
        $oauthClient = OAuthClient::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $oauthClientEntity->getIdentifier()
                    ]
                ]);
        if($oauthClient instanceof OAuthClient){
            $accessToken->oauthclient = $oauthClient->identifier;
//                $accessTokens = AccessToken::find([
//                            "client = :client:",
//                            'bind' =>[
//                                "oauthclient" => $oauthClient->getIdentifier()
//                            ]
//                        ]);
        }else{
            //Throw error no valid user or client
        }
//        foreach($accessTokens as $accessToken){
//            $accessToken->deleteRefreshTokens();
//            $accessToken->deleteScopes();
//            $accessToken->delete();
//        }
        if ($accessToken->save() === false) {
            echo "{'status':'failed','messages':[";

            $messages = $accessToken->getMessages();

            foreach ($messages as $message) {
                echo "'".$message."'";
            }
            echo "]}";
            die();
        }
        $scopeEntities = $accessTokenEntity->getScopes();
        $scopes = array();
        foreach ($scopeEntities as $scopeEntity){
            $scopes[] = $scopeEntity->getIdentifier();
        }
        $accessToken->addScopes($scopes);
    }

    /**
     * {@inheritdoc}
     */
    public function revokeAccessToken($tokenId)
    {
        // Some logic here to revoke the access token
        $accessToken = AccessToken::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $tokenId
                    ]
                ]);
        if($accessToken === FALSE){
            return TRUE;
        }
        $accessToken->deleteRefreshTokens();
        $accessToken->deleteScopes();
        $accessToken->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function isAccessTokenRevoked($tokenId)
    {
        $accessToken = AccessToken::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $tokenId
                    ]
                ]);
        if($accessToken === FALSE){
            return TRUE;
        }
        return false; // Access token hasn't been revoked
    }

    /**
     * {@inheritdoc}
     */
    public function getNewToken(ClientEntityInterface $oauthClientEntity, array $scopes, $oauthUserIdentifier = null)
    {
        $accessToken = new AccessTokenEntity();
        $accessToken->setClient($oauthClientEntity);
        foreach ($scopes as $scope) {
            $accessToken->addScope($scope);
        }
        $accessToken->setUserIdentifier($oauthUserIdentifier);

        return $accessToken;
    }
}
