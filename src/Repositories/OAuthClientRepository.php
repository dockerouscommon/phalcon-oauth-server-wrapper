<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Phalcon\OAuth2\Server\Repositories;

use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Phalcon\OAuth2\Server\Entities\OAuthClientEntity;
use Phalcon\OAuth2\Server\Models\OAuthClient;
use Phalcon\Db\Column;
use Phalcon\Mvc\User\Component;

class OAuthClientRepository extends Component implements ClientRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getClientEntity($oauthClientIdentifier, $grantType, $oauthClientSecret = null, $mustValidateSecret = true){
        $oauthClient = OAuthClient::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $oauthClientIdentifier
                    ]
                ]);
        if($oauthClient === FALSE){
            return;
        }
        if (
            $mustValidateSecret === true
            && $oauthClient->isConfidential === true
            && password_verify($oauthClientSecret, $oauthClient->secret) === false
        ) {
            return;
        }
        
        // Todo: Validate Grant Type

        $oauthClientEntity = new OAuthClientEntity();
        $oauthClientEntity->setIdentifier($oauthClient->identifier);
        $oauthClientEntity->setName($oauthClient->name);
        $oauthClientEntity->setRedirectUri($oauthClient->redirectUri);
        $oauthClientEntity->setSecret($oauthClient->secret, FALSE);
        $oauthClientEntity->setIsConfidential($oauthClient->isConfidential);
        return $oauthClientEntity;
    }
}
