<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Phalcon\OAuth2\Server\Repositories;

use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Phalcon\OAuth2\Server\Entities\RefreshTokenEntity;
use Phalcon\OAuth2\Server\Models\RefreshToken;
use Phalcon\OAuth2\Server\Models\AccessToken;

class RefreshTokenRepository implements RefreshTokenRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntity)
    {
        // Some logic to persist the refresh token in a database
        $refreshToken = RefreshToken::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $refreshTokenEntity->getIdentifier()
                    ]
                ]);
        if($refreshToken === FALSE){
            $refreshToken = new RefreshToken();
            $refreshToken->identifier = $refreshTokenEntity->getIdentifier();
        }
        $expiryDateTime = $refreshTokenEntity->getExpiryDateTime();
        if($expiryDateTime instanceof \DateTime){
            $refreshToken->expiryDateTime = $expiryDateTime->format('Y-m-d H:i:s');
        }else{
            //Throw invalid expiry error
        }
        $accessTokenEntity = $refreshTokenEntity->getAccessToken();
        $accessToken = AccessToken::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $accessTokenEntity->getIdentifier()
                    ]
                ]);
        if($accessToken === FALSE){
            // Throw missing access token error
        }
        $refreshToken->accessToken = $accessToken->identifier;
        $refreshToken->save();
    }

    /**
     * {@inheritdoc}
     */
    public function revokeRefreshToken($tokenId)
    {
        // Some logic to revoke the refresh token in a database
        $refreshToken = RefreshToken::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $tokenId
                    ]
                ]);
        if($refreshToken === FALSE){
            return TRUE;
        }
        $refreshToken->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function isRefreshTokenRevoked($tokenId)
    {
        $refreshToken = RefreshToken::findFirst([
                    "identifier = :identifier:",
                    'bind' =>[
                        "identifier" => $tokenId
                    ]
                ]);
        if($refreshToken === FALSE){
            return TRUE;
        }
        return false; // Access token hasn't been revoked
    }

    /**
     * {@inheritdoc}
     */
    public function getNewRefreshToken()
    {
        return new RefreshTokenEntity();
    }
}
