<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Phalcon\OAuth2\Server\Entities;

use League\OAuth2\Server\Entities\UserEntityInterface;

class OAuthUserEntity implements UserEntityInterface{
    
    use EntityTrait;
    
    protected $username;
    
    protected $password;

    public function setPassword($password, $no_hash = FALSE){
        if(empty($password) || !is_string($password)){
            // Throw bad password exception
        }
        if($no_hash === TRUE){
            $this->password = $password;
        }else{
            $this->password = password_hash($password, PASSWORD_BCRYPT);
        }
        return $this->password;
    }    
    
    public function verifyPassword($password){
        return password_verify($password, $this->password);
    }
    
    public function setUsername($username){
        $this->username = $username;
    }
    
    public function getUsername(){
        return $this->username;
    }
}
