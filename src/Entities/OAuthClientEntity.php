<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Phalcon\OAuth2\Server\Entities;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\Traits\ClientTrait;
use League\OAuth2\Server\Entities\Traits\EntityTrait;

class OAuthClientEntity implements ClientEntityInterface
{
    use EntityTrait, ClientTrait;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var integer
     */
    protected $isConfidential = 1;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setRedirectUri($uri)
    {
        $this->redirectUri = $uri;
    }
    
    public function setSecret($secret,$hash = TRUE){
        if(empty($secret) || !is_string($secret)){
            // Throw invalid exception
        }elseif(strlen($secret) < 32 ){
            // Throw too short exception
        }
        if($hash === FALSE){
            $this->secret = $secret;
        }else{
            $this->secret = password_hash($secret, PASSWORD_BCRYPT);
        }
        return $this->secret;
    }
    
    public function getSecret(){
        return $this->secret;
    }
    
    public function setIsConfidential($confidential = TRUE){
        if($confidential === FALSE || $confidential == 0){
            $this->isConfidential = 0;
        } else {
            $this->isConfidential = 1;
        }
    }
    
    public function getIsConfidential(){
        if($this->isConfidential === 0){
            return FALSE;
        }
        return TRUE;
    }
}
